#!/bin/sh

dir=$(cd $(dirname "$BASH_SOURCE}") && pwd)
cd "$dir"
# Install config.rpath which is needed for AM_ICONV macro
for dir in "$GETTEXT_DIR" /usr/share/gettext /usr/local/share/gettext; do
  if test -f "$dir/config.rpath"; then
    test -f config.rpath || echo "autogen.sh: installing './config.rpath'"
    cp -f "$dir/config.rpath" .
    break
  fi
done

aclocal --force
autoconf --force
if test -e Makefile.am; then
  automake --add-missing --copy --force-missing
fi
